package com.bilgeadam.app.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import com.google.gson.Gson



class RetrofitClient {

    companion object {

        private var retrofit:Retrofit? = null
        private var gson:Gson? = null


        fun getClient(baseUrl:String) : Retrofit? {
            if( retrofit == null) {

                if( gson == null ){
                    gson = GsonBuilder()
                            .setLenient()
                            .create()
                }

                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }
            return retrofit
        }

    }

}
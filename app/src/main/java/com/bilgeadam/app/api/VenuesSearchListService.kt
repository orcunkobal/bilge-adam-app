package com.bilgeadam.app.api

import com.bilgeadam.app.model.photos.PhotoResponse
import com.bilgeadam.app.model.tips.TipResponse
import com.bilgeadam.app.model.venues.VenueResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface VenuesSearchListService {

    @GET("venues/search/?=client_id=${ApiUtils.CLIENT_ID}&client_secret=${ApiUtils.CLIENT_SECRET}")
    fun getList(
                @Query("ll")
                ll:String,
                @Query("query")
                query:String,
                @Query("v")
                v:String
    ) : Call<VenueResponse>

    @GET
    fun getDetail(@Url url:String) : Call<TipResponse>

    @GET
    fun getPhoto(@Url url:String) : Call<PhotoResponse>

}
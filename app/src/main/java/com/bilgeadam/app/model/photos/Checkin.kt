package com.bilgeadam.app.model.photos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Checkin {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("createdAt")
    @Expose
    var createdAt: Int? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("timeZoneOffset")
    @Expose
    var timeZoneOffset: Int? = null

}

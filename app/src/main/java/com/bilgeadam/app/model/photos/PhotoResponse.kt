package com.bilgeadam.app.model.photos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PhotoResponse {

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
    @SerializedName("response")
    @Expose
    var response: Response? = null

}
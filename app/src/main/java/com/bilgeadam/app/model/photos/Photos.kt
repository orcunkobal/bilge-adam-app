package com.bilgeadam.app.model.photos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Photos {

    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("items")
    @Expose
    var items: List<Item>? = null
    @SerializedName("dupesRemoved")
    @Expose
    var dupesRemoved: Int? = null

}
package com.bilgeadam.app.model.photos


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName("photos")
    @Expose
    var photos: Photos? = null

}
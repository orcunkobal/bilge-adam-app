package com.bilgeadam.app.model.tips

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Likes {

    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("groups")
    @Expose
    var groups: List<Group>? = null
    @SerializedName("summary")
    @Expose
    var summary: String? = null

}
package com.bilgeadam.app.model.tips

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TipResponse {

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
    @SerializedName("response")
    @Expose
    var response: Response? = null

}
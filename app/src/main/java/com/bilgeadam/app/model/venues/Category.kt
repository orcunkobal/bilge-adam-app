package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Category(

        @SerializedName("id")
        @Expose
        var id:String,

        @SerializedName("name")
        @Expose
        var name:String,

        @SerializedName("pluralName")
        @Expose
        var pluralName:String,

        @SerializedName("shortName")
        @Expose
        var shortName:String,

        @SerializedName("icon")
        @Expose
        var icon: Icon,

        @SerializedName("primary")
        @Expose
        var primary:Boolean


)
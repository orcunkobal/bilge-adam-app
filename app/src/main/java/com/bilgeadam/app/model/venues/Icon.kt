package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Icon(

        @SerializedName("prefix")
        @Expose
        var prefix:String,

        @SerializedName("suffix")
        @Expose
        var suffix:String

)
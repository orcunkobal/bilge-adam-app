package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Location(

        @SerializedName("address")
        @Expose
        var address: String,

        @SerializedName("lat")
        @Expose
        var lat: Double,

        @SerializedName("lng")
        @Expose
        var lng: Double,

        @SerializedName("distance")
        @Expose
        var distance: Int,

        @SerializedName("postalCode")
        @Expose
        var postalCode: String,

        @SerializedName("cc")
        @Expose
        var cc: String,

        @SerializedName("city")
        @Expose
        var city: String,

        @SerializedName("state")
        @Expose
        var state: String,

        @SerializedName("country")
        @Expose
        var country: String,

        @SerializedName("formattedAddress")
        @Expose
        var formattedAddress: List<String>

)
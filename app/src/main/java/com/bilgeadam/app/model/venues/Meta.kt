package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Meta(

        @SerializedName("code")
        @Expose
        var code: Int,

        @SerializedName("requestId")
        @Expose
        var requestId: String

)
package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList


data class Response(

        @SerializedName("venues")
        @Expose
        var venues: ArrayList<Venue>

)
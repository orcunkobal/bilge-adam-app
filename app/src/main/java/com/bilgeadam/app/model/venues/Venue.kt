package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Venue(

        @SerializedName("id")
        @Expose
        var id: String,

        @SerializedName("name")
        @Expose
        var name: String,

        @SerializedName("location")
        @Expose
        var location: Location,

        @SerializedName("categories")
        @Expose
        var categories: List<Category>,

        @SerializedName("referralId")
        @Expose
        var referralId: String,

        @SerializedName("hasPerk")
        @Expose
        var hasPerk: Boolean



)
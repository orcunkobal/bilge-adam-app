package com.bilgeadam.app.model.venues

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VenueResponse(

        @SerializedName("meta")
        @Expose
        var meta: Meta,

        @SerializedName("response")
        @Expose
        var response: Response

)
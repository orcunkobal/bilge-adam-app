package com.bilgeadam.app.repository

import android.content.Context
import com.bilgeadam.app.api.ApiUtils
import com.bilgeadam.app.api.VenuesSearchListService
import com.bilgeadam.app.model.photos.PhotoResponse
import com.bilgeadam.app.model.tips.TipResponse
import com.bilgeadam.app.model.venues.VenueResponse
import retrofit2.Call
import retrofit2.Callback

class ListRepository(mContext: Context) {

    private var apiVenuesService: VenuesSearchListService = ApiUtils.getVenuesService()!!

    companion object {

        private  var listRepository: ListRepository? = null

        fun getInstance(mContext: Context) : ListRepository? {
            if( listRepository == null )
                listRepository = ListRepository(mContext)
            return listRepository
        }

        interface ApiCallBackVenue{
            fun onResponse(response: VenueResponse?)
            fun onError(message: String?)
        }

        interface ApiCallBackTip{
            fun onResponse(response: TipResponse?)
            fun onError(message: String?)
        }

        interface ApiCallBackPhoto{
            fun onResponse(response: PhotoResponse?)
            fun onError(message: String?)
        }

    }

    fun getVenueList(ll:String,query:String,v:String,callBack:ApiCallBackVenue) {
        apiVenuesService.getList(ll,query,v).enqueue(object : Callback<VenueResponse> {
            override fun onFailure(call: Call<VenueResponse>?, t: Throwable?) {
                callBack.onError(t?.message)
            }
            override fun onResponse(call: Call<VenueResponse>?, response: retrofit2.Response<VenueResponse>?) {
                callBack.onResponse(response?.body())
            }
        })
    }

    fun getVenueDetail(url:String,callBack:ApiCallBackTip){
        apiVenuesService.getDetail(url).enqueue(object : Callback<TipResponse> {
            override fun onFailure(call: Call<TipResponse>?, t: Throwable?) {
                callBack.onError(t?.message)
            }
            override fun onResponse(call: Call<TipResponse>?, response: retrofit2.Response<TipResponse>?) {
                val item = response?.body()
                callBack.onResponse(item)


            }
        })
    }

    fun getPhoto(urlPhoto: String, apiCallBackPhoto: ListRepository.Companion.ApiCallBackPhoto) {
        apiVenuesService.getPhoto(urlPhoto).enqueue(object : Callback<PhotoResponse> {
            override fun onFailure(call: Call<PhotoResponse>?, t: Throwable?) {
                apiCallBackPhoto.onError(t?.message)
            }
            override fun onResponse(call: Call<PhotoResponse>?, response: retrofit2.Response<PhotoResponse>?) {
                val item = response?.body()
                apiCallBackPhoto.onResponse(item)
            }
        })
    }

}
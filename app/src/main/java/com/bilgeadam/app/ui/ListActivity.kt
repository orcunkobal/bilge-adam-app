package com.bilgeadam.app.ui

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import com.bilgeadam.app.BaseActivity
import com.bilgeadam.app.R
import com.bilgeadam.app.api.ApiUtils
import com.bilgeadam.app.model.venues.Response
import com.bilgeadam.app.model.venues.Venue
import com.bilgeadam.app.utils.Constant
import com.bilgeadam.app.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_dialog.*
import java.lang.Exception


class ListActivity : BaseActivity() , MyOnItemClickListener {

    private lateinit var mListViewModel:ListViewModel
    private lateinit var txtQuery:String
    private lateinit var txtCity:String
    private lateinit var venueList:ArrayList<Venue>
    private lateinit var mAdapter: ListAdapter
    private var mDialog:Dialog? = null

    override fun getLayout(): Int {
        return R.layout.activity_list
    }

    override fun init() {
        initDialog()
        prepareToolbar()
        getDataFromExtras()
        initRecyclerView()
        initViewModel()
    }

    private fun initDialog() {
        if( mDialog == null )
            mDialog = Dialog(this)

        mDialog?.setContentView(R.layout.custom_dialog)
    }

    private fun initRecyclerView() {

        venueList = ArrayList()
        mAdapter = ListAdapter(venueList,this)

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter

    }

    override fun onItemClickListener(view: View?, position: Int) {

        val urlTip = "venues/${venueList[position].id}/tips/?=client_id=${ApiUtils.CLIENT_ID}&client_secret=${ApiUtils.CLIENT_SECRET}&v=${getApiVersion()}"
        val urlPhoto = "venues/${venueList[position].id}/photos/?=client_id=${ApiUtils.CLIENT_ID}&client_secret=${ApiUtils.CLIENT_SECRET}&v=${getApiVersion()}"

        mListViewModel.getVenueDetail(urlTip).observe(this, Observer {

            if( it != null ){

                try{

                    val data = it.response
                    val items = data?.tips?.items
                    val user = items?.get(0)?.user

                    val txtTitle = mDialog?.txtTitle as TextView
                    val txtComment = mDialog?.txtComment as TextView
                    val txtUser = mDialog?.txtUser as TextView
                    val imgPlace = mDialog?.imgPlace as ImageView

                    imgPlace.setImageResource(R.drawable.ic_launcher_background)
                    txtTitle.text = venueList[position].name
                    txtComment.text = items?.get(0)?.text
                    txtUser.text =  String.format("%s %s",
                            if( TextUtils.isEmpty(user?.firstName) ){ "" } else{user?.firstName},
                            if( TextUtils.isEmpty(user?.lastName) ){ "" } else{user?.lastName})

                    mDialog?.show()

                }catch (e : Exception){
                    e.printStackTrace()
                }


            }
        })

        mListViewModel.getPhoto(urlPhoto).observe(this, Observer {
            if( it != null ){
                try{

                    val photos = it.response?.photos?.items?.get(0)

                    val imgPlace = mDialog?.imgPlace as ImageView
                    Picasso.get().load(photos?.prefix + "300x300" + photos?.suffix).into(imgPlace)

                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

        })

    }

    private fun hideContent(hide:Boolean) {
        val txtTitle = mDialog?.txtTitle as TextView
        val txtComment = mDialog?.txtComment as TextView
        val txtUser = mDialog?.txtUser as TextView
        val imgPlace = mDialog?.imgPlace as ImageView

        var showHide = View.INVISIBLE
        if(hide)
            showHide = View.VISIBLE

        txtTitle.visibility = showHide
        txtComment.visibility = showHide
        txtUser.visibility = showHide
        imgPlace.visibility = showHide
    }

    private fun getDataFromExtras() {
        txtQuery = intent.getStringExtra(Constant.TXT_QUERY_KEY)
        txtCity = intent.getStringExtra(Constant.TXT_CITY_KEY)
    }

    private fun initViewModel() {
        loadingBar.visibility = View.VISIBLE
        mListViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        mListViewModel.venueList(txtCity,txtQuery,getApiVersion()).observe(this, Observer<Response>{
            try {
                loadingBar.visibility = View.GONE
                if( it?.venues != null ){
                    txtEmpty.visibility = View.GONE
                    mRecyclerView.visibility = View.VISIBLE
                    setDataToList(it.venues)
                }else{
                    txtEmpty.visibility = View.VISIBLE
                    mRecyclerView.visibility = View.GONE
                }

            }catch (e:Exception){
                e.printStackTrace()
            }
        })
    }

    private fun getApiVersion(): String {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = (calendar.get(Calendar.MONTH) % 12) + 1
        val newMonth = String.format("%02d", month)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        return "$year$day$newMonth"
    }

    private fun setDataToList(venues: ArrayList<Venue>) {
        venueList.addAll(venues)
        mAdapter.notifyDataSetChanged()
    }

    private fun prepareToolbar() {
        setSupportActionBar(mToolbarList)
        if( supportActionBar != null )
            supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

}

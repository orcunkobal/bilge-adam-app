package com.bilgeadam.app.ui

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bilgeadam.app.R
import com.bilgeadam.app.model.venues.Location
import com.bilgeadam.app.model.venues.Venue
import kotlinx.android.synthetic.main.list_row.view.*
import java.util.ArrayList

class ListAdapter(venueList: ArrayList<Venue>, onItemClickListener: MyOnItemClickListener) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {

    private var mVenueList = venueList
    private var mClickListener:MyOnItemClickListener = onItemClickListener

    override fun getItemCount(): Int {
        return mVenueList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.list_row,parent,false)
        return ListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.txtLocationName.text = prepareLocationName(mVenueList[position].location)
        holder.txtPlaceName.text = mVenueList[position].name
    }

    private fun prepareLocationName(location: Location): CharSequence? {

        var city = ""
        if( !TextUtils.isEmpty(location.city) )
            city = location.city

        var state = ""
        if( !TextUtils.isEmpty(location.state) )
            state = location.state


        var displayLocation = city
        if( !TextUtils.isEmpty(state) ){
            displayLocation +=  if( !TextUtils.isEmpty(city) ){ " / $state" }else{ state }
        }

        if( TextUtils.isEmpty(location.city) && TextUtils.isEmpty(location.state) ){
            displayLocation = location.country
        }

        return displayLocation
    }

    inner class ListViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) , View.OnClickListener {

        init {
            itemView?.setOnClickListener(this)
        }

        private var row = itemView as CardView
        var txtLocationName = row.txtLocationName!!
        var txtPlaceName = row.txtPlaceName!!

        override fun onClick(view: View?) {
            mClickListener.onItemClickListener(view,adapterPosition)
        }

    }

}
package com.bilgeadam.app.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import com.bilgeadam.app.BaseActivity
import com.bilgeadam.app.R
import kotlinx.android.synthetic.main.activity_main.*
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import com.bilgeadam.app.utils.Constant
import com.bilgeadam.app.utils.Utils
import java.util.*
import com.google.android.gms.location.*

class MainActivity : BaseActivity() {

    private val MY_PERMISSIONS_REQUEST_LOCATION = 99
    private var locationManager:LocationManager? = null
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION)

    private var hasGps = false
    private var hasNetwork = false

    private var etCityText = ""
    private var etQueryText = ""

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun init() {
        initLocationManager()
        checkLocationPermission()
        initToolbar()
        searchButton()
    }

    private fun initLocationManager() {
        if( locationManager == null )
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }


    private fun searchButton() {
        btn_search.setOnClickListener {

            etCityText      = et_city.text.toString().trim()
            etQueryText     = et_query.text.toString().trim()

            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
                val permissionOk = checkLocationPermission()
                if (!permissionOk) {
                    return@setOnClickListener
                }
            }

            val status = checkIfLocationEnabled()
            if (!status) {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                return@setOnClickListener
            }

            if( TextUtils.isEmpty(etQueryText) ){
                showAlertDialog(getString(R.string.txt_empty_category),getString(R.string.alert_btn_txt_ok))
                return@setOnClickListener
            }else{
                if( etQueryText.length < 3 ){
                    showAlertDialog(getString(R.string.txt_low_length_category),getString(R.string.alert_btn_txt_ok))
                    return@setOnClickListener
                }
            }

            if( TextUtils.isEmpty(etCityText) ){
                getLastLocation()
                return@setOnClickListener
            }else{
                if( etCityText.length < 3 ){
                    showAlertDialog(getString(R.string.txt_low_length_city),getString(R.string.alert_btn_txt_ok))
                    return@setOnClickListener
                }else{
                    convertCityToLL()
                    return@setOnClickListener
                }
            }

        }
    }

    private fun goToListActivity(){
        val extras = Bundle()
        extras.putString(Constant.TXT_QUERY_KEY,etQueryText)
        extras.putString(Constant.TXT_CITY_KEY,etCityText)
        Utils.changeActivity(this@MainActivity,ListActivity::class.java,extras)
        et_city.setText("")
        et_query.setText("")
    }

    private fun showAlertDialog(message:String,btn_positive_title:String){
        AlertDialog.Builder(this@MainActivity)
                .setTitle(getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(btn_positive_title) { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {

        val locationRequest = LocationRequest.create()
        locationRequest.interval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        fusedLocationClient.requestLocationUpdates(locationRequest, object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                val currentLocation = locationResult?.lastLocation as Location

                etCityText = "${currentLocation.latitude},${currentLocation.longitude}"
                goToListActivity()

                fusedLocationClient.removeLocationUpdates(this)
            }

        }, Looper.myLooper())

    }

    private fun checkIfLocationEnabled() : Boolean {
        hasGps = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if ( !hasGps && !hasNetwork) {
            return false
        }
        return true
    }

    private fun convertCityToLL() {
        val gc = Geocoder(this, Locale.getDefault())
        val ads = gc.getFromLocationName(etCityText, 1)
        if( ads.size > 0 ){
            etCityText = "${ads[0].latitude},${ads[0].longitude}"
            goToListActivity()
        }else{
            showAlertDialog(getString(R.string.txt_wrong_city_message),getString(R.string.alert_btn_txt_ok))
        }
    }

    private fun checkLocationPermission() : Boolean {
            if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION) &&
                        ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {

                    AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert_txt_location_title))
                            .setMessage(getString(R.string.alert_txt_location_message))
                            .setPositiveButton(getString(R.string.alert_btn_txt_positive)) { dialogInterface, i ->
                                ActivityCompat.requestPermissions(this@MainActivity,permissions,MY_PERMISSIONS_REQUEST_LOCATION)
                                dialogInterface.dismiss()
                            }.setNegativeButton(getString(R.string.alert_btn_txt_negative)) { dialogInterface, i -> dialogInterface.dismiss() }
                            .create()
                            .show()

                } else {
                    ActivityCompat.requestPermissions(this,permissions,MY_PERMISSIONS_REQUEST_LOCATION)
                }
                return false
            } else {
                return true
            }
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this@MainActivity,R.string.alert_txt_location_message,Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    private fun initToolbar() {
        setSupportActionBar(mToolbar)
        if( supportActionBar != null )
            supportActionBar!!.setDisplayShowTitleEnabled(false)
    }


}

package com.bilgeadam.app.ui

import android.view.View

interface MyOnItemClickListener{

    fun onItemClickListener(view: View?, position:Int)

}
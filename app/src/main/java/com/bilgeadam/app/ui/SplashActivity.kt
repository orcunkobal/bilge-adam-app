package com.bilgeadam.app.ui

import android.content.Intent
import android.os.CountDownTimer
import com.bilgeadam.app.BaseActivity
import com.bilgeadam.app.R

class SplashActivity : BaseActivity() {

    override fun getLayout(): Int {
        return R.layout.activity_splash
    }

    override fun init() {

        object : CountDownTimer(2000,1000) {
            override fun onFinish() {
                val intent = Intent(this@SplashActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onTick(p0: Long) {
            }

        }.start()

    }


}

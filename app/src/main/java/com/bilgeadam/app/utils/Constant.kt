package com.bilgeadam.app.utils

class Constant {

    companion object {

        val TXT_QUERY_KEY = "query"
        val TXT_CITY_KEY = "city"

    }

}
package com.bilgeadam.app.utils

import android.content.Intent
import android.app.Activity
import android.os.Bundle

class Utils{

    companion object {

        fun changeActivity(activity: Activity, goClass: Class<*>,extras: Bundle? = null) {
            val intent = Intent(activity, goClass)
            if( extras != null ){
                intent.putExtras(extras)
            }
            activity.startActivity(intent)
        }

    }

}
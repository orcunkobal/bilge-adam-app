package com.bilgeadam.app.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.bilgeadam.app.model.photos.PhotoResponse
import com.bilgeadam.app.model.tips.Item
import com.bilgeadam.app.model.tips.TipResponse
import com.bilgeadam.app.model.venues.VenueResponse
import com.bilgeadam.app.model.venues.Response
import com.bilgeadam.app.repository.ListRepository

class ListViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository:ListRepository = ListRepository.getInstance(application.applicationContext)!!

    private var mVenueList: MutableLiveData<Response> = MutableLiveData()

    private var mVenueDetail: MutableLiveData<TipResponse> = MutableLiveData()

    private var mVenuePhoto: MutableLiveData<PhotoResponse> = MutableLiveData()

    fun venueList(ll:String,query:String,v:String): MutableLiveData<Response> {
        mRepository.getVenueList(ll,query,v,object : ListRepository.Companion.ApiCallBackVenue{
            override fun onResponse(response: VenueResponse?) {
                mVenueList.postValue(response?.response)
            }
            override fun onError(message: String?) {
            }
        })
        return mVenueList
    }

    fun getVenueDetail(url:String) : MutableLiveData<TipResponse> {
        mRepository.getVenueDetail(url,object : ListRepository.Companion.ApiCallBackTip{
            override fun onResponse(response: TipResponse?) {
                mVenueDetail.postValue(response)
            }
            override fun onError(message: String?) {
            }
        })
        return mVenueDetail
    }

    fun getPhoto(urlPhoto: String): MutableLiveData<PhotoResponse> {
        mRepository.getPhoto(urlPhoto,object : ListRepository.Companion.ApiCallBackPhoto{
            override fun onResponse(response: PhotoResponse?) {
                mVenuePhoto.postValue(response)
            }

            override fun onError(message: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
        return mVenuePhoto
    }

}